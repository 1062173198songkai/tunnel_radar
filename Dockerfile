FROM python:3.9
WORKDIR /app
COPY . /app
RUN pip install -i https://pypi.tuna.tsinghua.edu.cn/simple -r requirements.txt --upgrade
RUN pip install yaml
EXPOSE 9999
CMD ["python", "tools/server.py"]

#FROM continuumio/miniconda3:latest
#WORKDIR /app
#COPY . /app
#RUN conda env create --force -f CondaEnv.yml; exit 0
##RUN pip install --no-dependencies --force-reinstall -r requirement.txt
#EXPOSE 9999
#CMD ["python", "tools/server.py"]
