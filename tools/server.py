#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 18:52:42 2020

@author: ansonwan
"""
from flask import Flask, request, jsonify
from flask_sugar import Sugar, Header, File, UploadFile
from pydantic import BaseModel
import json
import sys
import uuid
import os
# import inference as test
# import inference as test
from inference import valid,image_predict_data,mat_loader
from nacos_config import nacos_config, add_listener
from utils import create_folder_if_not_exist
import asyncio
sys.path.append('../')
app = Sugar(__name__)
app.debug = False

# @app.before_request
# async def nacos_listener():
#     asyncio.create_task(add_listener())

@app.route(rule="/api/analysis/inside", methods=['POST'])
def upload(file: UploadFile = File(...)):
    id = uuid.uuid4()
    static_home_dir = nacos_config.get_or_default(key='static-path', default='/Users/admin/PycharmProjects/pythonProject2/tools')
    dir = f"{static_home_dir}{os.sep}temData"
    create_folder_if_not_exist(static_home_dir)
    create_folder_if_not_exist(dir)
    path = f"{dir}{os.sep}{id}{file.filename}"
    file.save(path, buffer_size=16384)
    file.close()
    data_in = mat_loader(path)['E_obs']
    data_save_path = image_predict_data(f"{id}", data_in, path, "data")
    inv_save_path, ide_save_path, defect_names = valid(f"{id}", path)
    dataPModel = {
        "code": 200,
        "msg": "success",
        "data": {
            "id": id,
            "data_save_path": f"{nacos_config.get_or_default(key='domain', default='http://localhost/')}{data_save_path[len(static_home_dir) + 1:].replace(os.sep, '/')}",
            "inv_jpgPath": f"{nacos_config.get_or_default(key='domain', default='http://localhost/')}{inv_save_path[len(static_home_dir)+1:].replace(os.sep, '/')}",
            "ide_jpgPath": f"{nacos_config.get_or_default(key='domain', default='http://localhost/')}{ide_save_path[len(static_home_dir)+1:].replace(os.sep, '/')}",
            "defects_names": defect_names,
        }
    }
    return jsonify(dataPModel)




@app.route('/image/analysis', methods=['POST'])
def post_http():
    # if  not request.data:
    # return ('fail')
    params = request.data.decode('utf-8')
    params = json.loads(params)
    print(f"params:{params}")
    matPath = params["matPath"] if "matPath" in params else "test_predict_mat/inpu_data/8_predict.mat"
    inv_save_path, ide_save_path, defect_names = valid(matPath)
    dataPModel = {
        "code": 200,
        "msg": "success",
        "data": {
            "id": "skjkfdjkji110090900980sfsd",
            "inv_jpgPath": inv_save_path,
            "ide_jpgPath": ide_save_path,
            "defects_names": defect_names,
        }
    }
    return jsonify(dataPModel)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9527)
# defect_names = valid(observe_path)
# print(defect_names)
# print('all_time: {:.6f}'.format(all_time))
