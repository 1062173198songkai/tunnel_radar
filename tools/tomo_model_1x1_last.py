# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 16:38:56 2020

@author: lenovo
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 16:04:25 2020

@author: lenovo
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 15:52:02 2020

@author: lenovo
"""
import torch
import torch.nn as nn
import torch.nn.functional as F
from IPython.core import debugger
from torch.autograd import Variable
debug = debugger.Pdb().set_trace
device = torch.device("cuda")

class BiConvLSTMCell(nn.Module):

    def __init__(self, input_size, input_dim, hidden_dim, kernel_size, bias):
        """
        Initialize ConvLSTM cell.

        Parameters
        ----------
        input_size: (int, int)
            Height and width of input tensor as (height, width).
        input_dim: int
            Number of channels of input tensor.
        hidden_dim: int
            Number of channels of hidden state.
        kernel_size: (int, int)
            Size of the convolutional kernel.
        bias: bool
            Whether or not to add the bias.
        """

        super(BiConvLSTMCell, self).__init__()

        self.height, self.width = input_size
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim

        self.kernel_size = kernel_size
        # NOTE: This keeps height and width the same
        self.padding = kernel_size[0] // 2, kernel_size[1] // 2
        self.bias = bias

        self.conv = nn.Conv2d(in_channels=self.input_dim + self.hidden_dim,
                              out_channels=4 * self.hidden_dim,
                              kernel_size=self.kernel_size,
                              padding=self.padding,
                              bias=self.bias)

        # TODO: we may want this to be different than the conv we use inside each cell
        self.conv_concat = nn.Conv2d(in_channels=self.input_dim + self.hidden_dim,
                                     out_channels=self.hidden_dim,
                                     kernel_size=self.kernel_size,
                                     padding=self.padding,
                                     bias=self.bias)

    def forward(self, input_tensor, hidden_state, cell_state):
        h_cur = hidden_state
        h_cur = h_cur.to(device)
        c_cur = cell_state

        combined = torch.cat([input_tensor, h_cur], dim=1)  # concatenate along channel axis
        combined_conv = self.conv(combined)
        cc_i, cc_f, cc_o, cc_g = torch.split(combined_conv, self.hidden_dim, dim=1)
        i = torch.sigmoid(cc_i)
        f = torch.sigmoid(cc_f)
        o = torch.sigmoid(cc_o)
        g = torch.tanh(cc_g)
        c_next = f * c_cur + i * g
        h_next = o * torch.tanh(c_next)
        return h_next, c_next
    def init_hidden(self, batch_size, hidden, shape):


        # return (Variable(torch.zeros(batch_size, hidden, shape[0], shape[1])).cuda(),
        #         Variable(torch.zeros(batch_size, hidden, shape[0], shape[1])).cuda())
        return (Variable(torch.zeros(batch_size, hidden, shape[0], shape[1])).to(device),
                Variable(torch.zeros(batch_size, hidden, shape[0], shape[1])).to(device))
        
class BottleneckLSTM(nn.Module):
  	def __init__(self, input_channels, hidden_channels, kernel_size,height, width):
  		""" Creates Bottleneck LSTM layer
  		Arguments:
  			input_channels : variable having value of number of channels of input to this layer
  			hidden_channels : variable having value of number of channels of hidden state of this layer
  			height : an int variable having value of height of the input
  			width : an int variable having value of width of the input
  			batch_size : an int variable having value of batch_size of the input
  		Returns:
  			Output tensor of LSTM layer
  		"""
  		super(BottleneckLSTM, self).__init__()
  		self.input_channels = int(input_channels)
  		self.hidden_channels = int(hidden_channels)
  		self.height = height
  		self.width = width
  		self.kernel_size = kernel_size
  		self.cell = BiConvLSTMCell(input_size=(self.height, self.width),
                                              input_dim=self.input_channels,
                                              hidden_dim=self.hidden_channels,
                                              kernel_size=self.kernel_size,
                                              bias=True)
  		#self.batch_size = batch_size
  		
  
  	def forward(self, input,set_hideen_state,set_cell_state):
          bsize,_,_,_, = input.size()
          if set_hideen_state == True:
              (h1, c1) = self.cell.init_hidden(bsize, hidden=self.hidden_channels, shape=(self.height, self.width))
  			#debug()
              self.hidden_state = h1
              self.cell_state = c1
  			#debug()
              new_h, new_c = self.cell(input, self.hidden_state, self.cell_state)
              self.hidden_state = new_h
              self.cell_state = new_c
          else:
  			#debug()
              new_h, new_c = self.cell(input, self.hidden_state, self.cell_state)
              self.hidden_state = new_h
              self.cell_state = new_c
          return self.hidden_state,self.cell_state

class BottleneckLSTM2(nn.Module):
  	def __init__(self, input_channels, hidden_channels, kernel_size,height, width):
  		""" Creates Bottleneck LSTM layer
  		Arguments:
  			input_channels : variable having value of number of channels of input to this layer
  			hidden_channels : variable having value of number of channels of hidden state of this layer
  			height : an int variable having value of height of the input
  			width : an int variable having value of width of the input
  			batch_size : an int variable having value of batch_size of the input
  		Returns:
  			Output tensor of LSTM layer
  		"""
  		super(BottleneckLSTM2, self).__init__()
  		self.input_channels = int(input_channels)
  		self.hidden_channels = int(hidden_channels)
  		self.height = height
  		self.width = width
  		self.kernel_size = kernel_size
  		self.cell = BiConvLSTMCell(input_size=(self.height, self.width),
                                              input_dim=self.input_channels,
                                              hidden_dim=self.hidden_channels,
                                              kernel_size=self.kernel_size,
                                              bias=True)
  		#self.batch_size = batch_size
  		
  
  	def forward(self, input,set_hideen_state,set_cell_state,pre_hidden,pre_cell,):
          bsize,_,_,_, = input.size()
          if set_hideen_state == True:
              (h1, c1) = (pre_hidden,pre_cell)
              #debug()
              self.hidden_state = h1
              self.cell_state = c1
  			#debug()
              new_h, new_c = self.cell(input, self.hidden_state, self.cell_state)
              self.hidden_state = new_h
              self.cell_state = new_c
          else:
  			#debug()
              new_h, new_c = self.cell(input, self.hidden_state, self.cell_state)
              self.hidden_state = new_h
              self.cell_state = new_c
          return self.hidden_state,self.cell_state

class Single_level_densenet(nn.Module):
    def __init__(self, filters, num_conv=4):
        super(Single_level_densenet, self).__init__()
        self.num_conv = num_conv
        self.conv_list = nn.ModuleList()
        self.bn_list = nn.ModuleList()
        for i in range(self.num_conv):
            self.conv_list.append(nn.Conv2d(filters, filters, 3, padding=1))
            self.bn_list.append(nn.BatchNorm2d(filters))

    def forward(self, x):
        outs = []
        outs.append(x)
        for i in range(self.num_conv):
            temp_out = self.conv_list[i](outs[i])
            if i > 0:
                for j in range(i):
                    temp_out += outs[j]
            outs.append(F.relu(self.bn_list[i](temp_out)))
        out_final = outs[-1]
        del outs
        return out_final


class Down_sample(nn.Module):
    def __init__(self, kernel_size=2, stride=2):
        super(Down_sample, self).__init__()
        self.down_sample_layer = nn.MaxPool2d(kernel_size, stride)

    def forward(self, x):
        y = self.down_sample_layer(x)
        return y, x


class Upsample_n_Concat(nn.Module):
    def __init__(self, filters):
        super(Upsample_n_Concat, self).__init__()
        self.upsample_layer = nn.ConvTranspose2d(filters, filters, 4, padding=1, stride=2)
        self.conv = nn.Conv2d(2 * filters, filters, 3, padding=1)
        self.bn = nn.BatchNorm2d(filters)

    def forward(self, x, y):
        x = self.upsample_layer(x)
        x = torch.cat([x, y], dim=1)
        x = F.relu(self.bn(self.conv(x)))
        return x


class Dense_Unet_Encoder(nn.Module):
    def __init__(self):
        num_conv = 4
        filters = 64
        in_chan = 1
        out_chan = 9
        super(Dense_Unet_Encoder, self).__init__()
        self.conv1 = nn.Conv2d(in_chan, filters, 1)
        self.d1 = Single_level_densenet(filters, num_conv)
        self.down1 = Down_sample()
        self.d2 = Single_level_densenet(filters, num_conv)
        self.down2 = Down_sample()
        self.d3 = Single_level_densenet(filters, num_conv)
        self.down3 = Down_sample()
        self.d4 = Single_level_densenet(filters, num_conv)
        self.down4 = Down_sample()
        self.bottom = Single_level_densenet(filters, num_conv)
        self.up4 = Upsample_n_Concat(filters)
        self.u4 = Single_level_densenet(filters, num_conv)
        self.up3 = Upsample_n_Concat(filters)
        self.u3 = Single_level_densenet(filters, num_conv)
        self.up2 = Upsample_n_Concat(filters)
        self.u2 = Single_level_densenet(filters, num_conv)
        self.up1 = Upsample_n_Concat(filters)
        self.u1 = Single_level_densenet(filters, num_conv)
    def forward(self, x):
        #bsz = x.shape[0]
        x = self.conv1(x)
        x, y1 = self.down1(self.d1(x))
        x, y2 = self.down1(self.d2(x))
        x, y3 = self.down1(self.d3(x))
        x, y4 = self.down1(self.d4(x))
        x = self.bottom(x)
        return x,y4,y3,y2,y1
    
       
    
class Dense_Unet_Decoder(nn.Module):
    def __init__(self):
        num_conv = 4
        filters = 64
        in_chan = 1
        out_chan = 9
        super(Dense_Unet_Decoder, self).__init__()
        self.conv1 = nn.Conv2d(in_chan, filters, 1)
        self.d1 = Single_level_densenet(filters, num_conv)
        self.down1 = Down_sample()
        self.d2 = Single_level_densenet(filters, num_conv)
        self.down2 = Down_sample()
        self.d3 = Single_level_densenet(filters, num_conv)
        self.down3 = Down_sample()
        self.d4 = Single_level_densenet(filters, num_conv)
        self.down4 = Down_sample()
        self.bottom = Single_level_densenet(filters, num_conv)
        self.up4 = Upsample_n_Concat(filters)
        self.u4 = Single_level_densenet(filters, num_conv)
        self.up3 = Upsample_n_Concat(filters)
        self.u3 = Single_level_densenet(filters, num_conv)
        self.up2 = Upsample_n_Concat(filters)
        self.u2 = Single_level_densenet(filters, num_conv)
        self.up1 = Upsample_n_Concat(filters)
        self.u1 = Single_level_densenet(filters, num_conv)
    def forward(self, x,y4,y3,y2,y1):
        #bsz = x.shape[0]
        x = self.u4(self.up4(x, y4))
        x = self.u3(self.up3(x, y3))
        x = self.u2(self.up2(x, y2))
        x = self.u1(self.up1(x, y1))

        return x
class LSTM_Feature(nn.Module):
    def __init__(self):
        super(LSTM_Feature, self).__init__()
        self.encoder = Dense_Unet_Encoder()
        self.decoder = Dense_Unet_Decoder()
    def forward(self, seq):
    	
        seq = F.interpolate(seq, size=[128, 128],mode="bilinear",align_corners=True)
        #debug()
        x,y4,y3,y2,y1 = self.encoder(seq)
        #debug()
        output = self.decoder(x,y4,y3,y2,y1)
        out_feature = F.interpolate(output, size=[60, 100],mode="bilinear",align_corners=True)
        return out_feature

    def detach_hidden(self):
        self.bottleneck_lstm.hidden_state.detach_()
        self.bottleneck_lstm.cell_state.detach_()     
    
class LSTM_Dense_Unet(nn.Module):
    def __init__(self):
        super(LSTM_Dense_Unet, self).__init__()
        #self.temporal = temporal
        self.Denseunet_feature = LSTM_Feature()
        self.conv_seg = nn.Conv2d(64, 9, kernel_size=1, stride=1)
        self.conv_inv = nn.Conv2d(64, 1, kernel_size=1, stride=1)
        self.conv_bn = nn.BatchNorm2d(64)
        self.sigmoid = nn.Sigmoid()
        self.bottleneck_lstm = BottleneckLSTM(input_channels=64,hidden_channels=64,kernel_size=[3,3],height=60,width=100)
        self.bottleneck_lstm2 = BottleneckLSTM2(input_channels=64,hidden_channels=64,kernel_size=[3,3],height=60,width=100)
        self.conv_concat = nn.Conv2d(in_channels=64 + 64,
                                     out_channels=64,
                                     kernel_size=3,
                                     padding=1,
                                     bias=True)
        #self.out = nn.Conv2d(ngf, output_nc, kernel_size=3, stride=1, padding=1)
    def forward(self, x):
        """
        :param x:  5D tensor    bz * temporal * 4 * 240 * 240
        :return:
        """
        Dense_feature_list = []
        ########################forward
        first_forward_inv = []
        first_forward_seg =[]
        h_state_forward = []
        cell_state_forward = []
        ####################h_state
        h_state_backward = []
        cell_state_backward = []
        #####################last
        last_output_inner = []
        last_output_inv = []
        last_output_seg = []
        
        _,_,temporal,_,_,= x.size()
        for t in range(temporal):
            #debug()
            im_t = x[:, :,t, :, :]             # bz * 4 * 240 * 240
            Dense_feature = self.Denseunet_feature(im_t)
            Dense_feature_list.append(Dense_feature)
            if t ==0:
                hidden_forward,cell_forward = self.bottleneck_lstm(Dense_feature,set_hideen_state=True,set_cell_state=True)
            else:
                hidden_forward,cell_forward = self.bottleneck_lstm(Dense_feature,set_hideen_state=False,set_cell_state=False)
            #debug()
            
            out_t1_inv = self.sigmoid(self.conv_inv(hidden_forward))
            out_t1_seg = self.conv_seg(hidden_forward) 
            h_state_forward.append(hidden_forward)
            cell_state_forward.append(cell_forward)
            
        ALL_Dense_feature = torch.stack(Dense_feature_list, dim=1)
        all_forward_hidden_state = torch.stack(h_state_forward, dim=1)
        #debug()
        all_forward_cell_state = torch.stack(cell_state_forward, dim=1)
        for t in range(temporal):
            if t ==0:
                hidden_backward,cell_backward = self.bottleneck_lstm2(ALL_Dense_feature[:,temporal - t - 1,:,:,:,],set_hideen_state=True,set_cell_state=True,pre_hidden=all_forward_hidden_state[:,temporal-1,:,:,:,],pre_cell=all_forward_cell_state[:,temporal-1,:,:,:,])
            else:
                hidden_backward,cell_backward = self.bottleneck_lstm2(ALL_Dense_feature[:,temporal - t - 1,:,:,:,],set_hideen_state=False,set_cell_state=False,pre_hidden=None,pre_cell=None,)
            
            h_state_backward.append(hidden_backward)
            cell_state_backward.append(cell_backward)
        all_backward_hidden_state = torch.stack(h_state_backward, dim=1)
        all_backward_cell_state = torch.stack(cell_state_backward, dim=1)  
        #debug()
        for t in range(temporal):
                h = self.conv_concat(torch.cat((all_forward_hidden_state[:,t,:,:,:], all_backward_hidden_state[:,temporal - t - 1,:,:,:]), dim=1))
                h = F.relu(self.conv_bn(h))
                out_t_inv = self.sigmoid(self.conv_inv(h))              # bz * 5 * 240 * 240
                out_t_seg = self.conv_seg(h) 
                last_output_inv.append(out_t_inv)
                last_output_seg.append(out_t_seg)
        return torch.stack(last_output_inv, dim=1), torch.stack(last_output_seg, dim=1)

