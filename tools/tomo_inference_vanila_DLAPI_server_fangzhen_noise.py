# -*- coding: utf-8 -*-
"""
Created on Sun Aug 29 10:20:59 2021

@author: lenovo
"""

import os
import sys
import time
import math
import random
import socket
import threading
import numpy as np
from datetime import datetime
import torch.nn.functional as F
import scipy.io as sio
import torch
import torch.nn as nn
import torch.optim as optim
import torch.backends.cudnn as cudnn
from torch.autograd import Variable, Function
from tensorboardX import SummaryWriter

#import basic_function as func
import tomo_dataset as dataset
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import os

from flask import Flask, request, jsonify
import json
app = Flask(__name__)
app.debug = True

from IPython.core import debugger
debug = debugger.Pdb().set_trace

os.environ["CUDA_VISIBLE_DEVICES"] = '0'

args_seed = 666
args_works = 0
observe_path ='D:\\Users\\test\\Fangzhen_Noise\\input\\2.mat'
path1= 'D:\\Users\\test\\Fangzhen_Noise\\output'
args_gpu = True
#args_batch = 1
#temporal = 4
mode = 'tomo'
#num_class = 9
if mode == 'tomo':
    import tomo_model_1x1_last as model
else:
    import inversenet_model2 as model

random.seed(args_seed)
torch.manual_seed(args_seed)
torch.cuda.manual_seed_all(args_seed)
#cudnn.deterministic = True
cudnn.benchmark = True
device = torch.device("cuda")
#
#valid_dataset = dataset.TestDatasetFolder(args_valid, flip=False, norm=True)
#valid_loader = torch.utils.data.DataLoader(
#        valid_dataset, batch_size=args_batch, shuffle=False,
#        pin_memory=True, drop_last=False)

if mode == 'tomo':
    G = model.LSTM_Dense_Unet().to(device)      
else:
    NetWorks = model.InverseNet().to(device)    
G = nn.DataParallel(G, device_ids=range(torch.cuda.device_count()))
checkpoint_file_G = torch.load('D:\\Users\\GPR_inv_ide\\model_SNR\\minloss_checkpoint.pth.tar')
G.load_state_dict(checkpoint_file_G['state_dict_g'])

class_names= ['Iron','Background', 'Interface', 'Anhydrous crack', 'Anhydrous void', 'Anhydrous delamination', 
               'Water-bearing delamination','Water-bearing crack','Water-bearing void', ]
########################################################
##################load the data
################
def mat_loader(path):
    try:
        data = sio.loadmat(path,verify_compressed_data_integrity=False) 
        return data 
               
    except Exception:
        print(path)


def image_observe(observe,observe_path,postfix):
    predict_mat = observe*2.4771214
    data = observe
    new_file_name = observe_path.split('.')[0][-1]
    #filename = tpath[0:tpath.rfind('.')] + '_' + postfix + '.mat'
    plt.figure(figsize=(5, 1), dpi=512)

    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=-0.2, vmax=0.2)
    # vmax = np.amax(np.abs(output))
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    #debug()
    # plt.colorbar()
    plt.savefig(path1 + '\\%06d' % int(new_file_name)+'_' + postfix+'.jpg')
    inv_path = path1 + '\\%06d' % int(new_file_name)+'_' + postfix+'.jpg'
    return inv_path
        
#############################################################   
def image_predict_inv(predict_mat,observe_path,postfix):
    predict_mat = predict_mat*2.4771214
    data = np.array(np.power(10,predict_mat))
    new_file_name = observe_path.split('.')[0][-1]
    #filename = tpath[0:tpath.rfind('.')] + '_' + postfix + '.mat'
    plt.figure(figsize=(5, 1), dpi=512)

    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=0, vmax=20)
    # vmax = np.amax(np.abs(output))
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    #debug()
    # plt.colorbar()
    plt.savefig(path1 + '\\%06d' % int(new_file_name)+'_' + postfix+'.jpg')
    inv_path = path1 + '\\%06d' % int(new_file_name)+'_' + postfix+'.jpg'
    return inv_path
def image_predict_segment(predict_semantic_mat,observe_path,postfix):
    data = np.array(predict_semantic_mat)
    new_file_name = observe_path.split('.')[0][-1]
    plt.figure(figsize=(5, 1), dpi=512)

    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=0, vmax=8)
    # vmax = np.amax(np.abs(output))
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    # plt.colorbar()
    plt.savefig(path1 + '\\%06d' % int(new_file_name)+'_' + postfix+'.jpg')
    ide_path = path1 + '\\%06d' % int(new_file_name)+'_' + postfix+'.jpg'
    return ide_path
    
def slide_window_slice(observe,window=50,step=10):
    _,_,h,w = observe.size()
    observe_clip = []
    observe_fill = []
    if (w-50)%10==0:
        times = int(((w-window)/step)+1)
        for j in range(times):
            observe_slice= observe[:,:,:,0+j*step:window+j*step]
            observe_clip.append(observe_slice)
        #debug()
    else:
        com_slice = (w-50)//10
        remain_slice = (w-50)%10
        observe_fill.append(observe)
        #debug()
        for i in range(step-remain_slice):
            observe_fill.append(observe[:,:,:,-1].unsqueeze(3))
        observe_filled = torch.cat(observe_fill, dim=3).cuda()
        #debug()
        _,_,hh,ww = observe_filled.size()
        times = int(((ww-window)/step)+1)
        assert com_slice+2 == times
        for j in range(times):
            observe_slice= observe_filled[:,:,:,0+j*step:window+j*step]
            observe_clip.append(observe_slice)
        
    return torch.stack(observe_clip, dim=2).cuda()    
##############################################################
#############################inference
######################
def valid(observe_path):
    G.eval()
    with torch.no_grad():
        end=time.time()
        #for step, (observe, observe_path) in enumerate(valid_loader):
            #debug()
        observe= mat_loader(observe_path)['E_obs']
        observe = torch.from_numpy(observe)
        observe = torch.unsqueeze(observe,0)
        observe = torch.unsqueeze(observe,0)
        #debug()
        observe = 2 * (observe + 6.957055) / (6.957055 + 7.815264) - 1
        observe1 = ((observe + 1) * (6.957055 + 7.815264) / 2) - 6.957055
        #observe1 = observe
        #((a.E_obs + 1) * (6.957055 + 7.815264) / 2) - 6.957055
        observe = observe.to(device)
        observe = slide_window_slice(observe,window=50,step=10)
        #debug()
        predict,predict_semantic= G(observe)
        _,_,temporal,_,_,= observe.size()
        
        all_predict_inv = []
        all_predict_seg = []
        defect_names = []
        for t in range(temporal):
            if t !=temporal-1:
                if t ==0:
                    predict_all = predict[:, t,:,:,:,]
                    predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                    predict_clip = predict_all[:,:,:,:20]
                    predict_semantic_clip = predict_semantic_70_all[:,:,:,:20]
                    all_predict_inv.append(predict_clip)
                    all_predict_seg.append(predict_semantic_clip)
                else:
                    predict_all = predict[:, t,:,:,:,]
                    predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                    all_predict_inv.append(0.5*(predict_all[:,:,:,:20]+predict[:, t-1,:,:,:,][:,:,:,20:40]))
                    all_predict_seg.append(0.5*(predict_semantic_70_all[:,:,:,:20]+predict_semantic[:, t-1,:,:,:,][:,:,:,20:40]))
            
            else:
                #debug()
                predict_all = predict[:, t,:,:,:,]
                predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                predict_all1 = 0.5*(predict_all[:,:,:,:20]+predict[:, t-1,:,:,:,][:,:,:,20:40])
                predict_all2 = predict_all[:,:,:,20:]
                predict_semantic_70_all1 = 0.5*(predict_semantic_70_all[:,:,:,:20]+predict_semantic[:, t-1,:,:,:,][:,:,:,20:40])
                predict_semantic_70_all2 = predict_semantic_70_all[:,:,:,20:]
                all_predict_inv.append(predict_all1)
                all_predict_inv.append(predict_all2)
                all_predict_seg.append(predict_semantic_70_all1)
                all_predict_seg.append(predict_semantic_70_all2)
                
        predict_inv_70_200 = torch.cat(all_predict_inv, dim=3).cuda()
        #debug()
        predict_semantic_70_200 = torch.cat(all_predict_seg, dim=3).cuda()
        predict3 = predict_semantic_70_200.max(1, keepdim=True)[1].byte()
        inv_save_path = image_predict_inv(predict_inv_70_200.squeeze().cpu().numpy(), observe_path, 'inv')
        ide_save_path = image_predict_segment(predict3.squeeze().cpu().numpy(), observe_path, 'semantic')
        image_observe_path = image_observe(observe1.squeeze().cpu().numpy(), observe_path, 'obs')
        
        _, preds = torch.max(predict_semantic_70_200, 1)
        class_preds = torch.unique(preds)
        #print(class_preds)
        #debug()
        for  i in class_preds:
            #debug()
            if i ==0:
                continue
            if i ==1:
                continue
            if i ==2:
                continue
            defect_name = class_names[int(i)]
            #if defect_name == ""
            defect_names.append(defect_name)
        #print(defect_names)
            
            
        all_time=time.time()-end
    
    return image_observe_path,inv_save_path,ide_save_path,defect_names


obs_path,inv_save_path,ide_save_path,defect_names = valid(observe_path)
print(defect_names)
@app.route('/image/analysis', methods=['GET', 'POST'])
def post_http():
    #if  not request.data:
        #return ('fail')

    params = request.data.decode('utf-8')
    params = json.loads(params)
    #print(params)
    matPath = params['filePath']
    #print(matPath)
    image_observe_path,inv_save_path,ide_save_path,defect_names = valid(matPath)
    defect_names = json.dumps(defect_names)  # 将python数据转为json
    dataPModel = {
        "code": 200,
        "msg": "success",
        "data": {
            "id": "skjkfdjkji110090900980sfsd",
            "inputPath": image_observe_path,
            "picPath": inv_save_path,
            "segPath": ide_save_path,
            "damageEntitys": [
                {
                    "id": 38473874839234344,
                    "sort": "1",
                    "position": "20-340cm",
                    "region": "拱肩",
                    "type": defect_names,
                    "size": "320cm",
                    "degree": "中等",
                    "depth": "21cm,40cm",
                    "remarks": "备注",
                }
            ]

        }
    }

    #dataPModel = json.dumps(dataPModel)  # 将python数据转为json

    # 把区获取到的数据转为JSON格式。
    #return jsonify(prams)

    return jsonify(dataPModel)




if __name__ == '__main__':
    app.run(host='127.0.0.1',port=2009)
#defect_names = valid(observe_path)
#print(defect_names)
#print('all_time: {:.6f}'.format(all_time))

