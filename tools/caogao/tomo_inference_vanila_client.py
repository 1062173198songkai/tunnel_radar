# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 08:36:57 2021

@author: liuhanchi
"""

import requests,json

def json_send(dataPModel,url):
    headers = {"Content-type": "application/json", "Accept": "text/plain", "charset": "UTF-8"}
    response = requests.post(url=url, headers=headers, data=json.dumps(dataPModel))
    return json.loads(response.text)
if __name__ == "__main__":
    url = 'http://127.0.0.1:3792//predict/'
    dataPModel = {
        "observe_path":"observe_path ='D:\\Users\\GPR_inv_test\\test\\inpu_data\\8.mat'"
    }
    
    result = json_send(dataPModel,url)
    #result = json.loads(result.text)
    
    print(result['class_result'])