# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 19:09:15 2020

@author: lenovo
"""


import os
import sys
import time
import random
import socket
import threading
import numpy as np
from datetime import datetime
import torch.nn.functional as F

import torch
import torch.nn as nn
import torchnet as tnt
import torch.optim as optim
import torch.backends.cudnn as cudnn
from torch.autograd import Variable, Function
from tensorboardX import SummaryWriter

import basic_function as func
import tomo_dataset as dataset

from IPython.core import debugger
debug = debugger.Pdb().set_trace

os.environ["CUDA_VISIBLE_DEVICES"] = '1'

args_seed = 666
args_works = 0
args_valid ='/root/saves/lhc_projects/lhc/code/LSTM/ceshi_damoxing/test'
args_gpu = True
args_batch = 1
#temporal = 4
mode = 'tomo'
num_class = 9
if mode == 'tomo':
    import tomo_model_1x1_last as model
else:
    import inversenet_model2 as model

random.seed(args_seed)
torch.manual_seed(args_seed)
torch.cuda.manual_seed_all(args_seed)
#cudnn.deterministic = True
cudnn.benchmark = True
device = torch.device("cuda")

valid_dataset = dataset.TestDatasetFolder(args_valid, flip=False, norm=True)
valid_loader = torch.utils.data.DataLoader(
        valid_dataset, batch_size=args_batch, shuffle=False,
        pin_memory=True, drop_last=False)

if mode == 'tomo':
    G = model.LSTM_Dense_Unet().to(device)      
else:
    NetWorks = model.InverseNet().to(device)    
G = nn.DataParallel(G, device_ids=range(torch.cuda.device_count()))
checkpoint_file_G = torch.load('/root/files/lhc/code/LSTM/A_ontime/L1_SSIM/Suiji_Sequence_3thmode/LSTM_TWICE_CELL_fuyong/model0/tomo_main_vanila_10x1_1_55dai/minloss_checkpoint.pth')
G.load_state_dict(checkpoint_file_G['state_dict_g'])

L1 = nn.L1Loss()
L2 = nn.MSELoss()
Edge = func.SoftIOU()
SSIM = func.SSIM()
MSSIM = func.MSSSIM()
class_total = list(0. for i in range(num_class))
class_total =torch.Tensor(class_total)

def valid(valid_loader, G):
    G.eval()
    with torch.no_grad():
        end=time.time()
        precision = list(0. for i in range(num_class))
        precision=torch.Tensor(precision)
        recall = list(0. for i in range(num_class)) 
        recall=torch.Tensor(recall)
        for step, (observe, geology, observe_path, geology_path,semantic) in enumerate(valid_loader):
            #debug()
            observe, geology = observe.to(device), geology.to(device)
            observe = observe.to(device)
            geology = geology.to(device)
            semantic = semantic.to(device)
            observe = func.slide_window_slice(observe,window=50,step=10)
            geology = func.slide_window_slice(geology,window=100,step=20)
            semantic = func.slide_window_slice(semantic,window=100,step=20)
            #debug()
            predict,predict_semantic= G(observe)
            _,_,temporal,_,_,= observe.size()
            
            all_inv = []
            all_seg =[]
            all_predict_inv = []
            all_predict_seg = []
            for t in range(temporal):
                if t !=temporal-1:
                    if t ==0:
                        predict_all = predict[:, t,:,:,:,]
                        predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                        predict_clip = predict_all[:,:,:,:20]
                        predict_semantic_clip = predict_semantic_70_all[:,:,:,:20]
                        all_predict_inv.append(predict_clip)
                        all_predict_seg.append(predict_semantic_clip)
                        all_inv.append(geology[:,:,t, :,:][:,:,:,:20])
                        all_seg.append(semantic[:,:, t,:,:][:,:,:,:20])
                    else:
                        predict_all = predict[:, t,:,:,:,]
                        predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                        all_predict_inv.append(0.5*(predict_all[:,:,:,:20]+predict[:, t-1,:,:,:,][:,:,:,20:40]))
                        all_predict_seg.append(0.5*(predict_semantic_70_all[:,:,:,:20]+predict_semantic[:, t-1,:,:,:,][:,:,:,20:40]))
                        all_inv.append(geology[:,:,t, :,:][:,:,:,:20])
                        all_seg.append(semantic[:,:,t, :,:][:,:,:,:20])
                
                else:
                    #debug()
                    predict_all = predict[:, t,:,:,:,]
                    predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                    predict_all1 = 0.5*(predict_all[:,:,:,:20]+predict[:, t-1,:,:,:,][:,:,:,20:40])
                    predict_all2 = predict_all[:,:,:,20:]
                    predict_semantic_70_all1 = 0.5*(predict_semantic_70_all[:,:,:,:20]+predict_semantic[:, t-1,:,:,:,][:,:,:,20:40])
                    predict_semantic_70_all2 = predict_semantic_70_all[:,:,:,20:]
                    all_predict_inv.append(predict_all1)
                    all_predict_inv.append(predict_all2)
                    all_predict_seg.append(predict_semantic_70_all1)
                    all_predict_seg.append(predict_semantic_70_all2)
                    all_inv.append(geology[:,:,t, :,:])
                    all_seg.append(semantic[:,:, t,:,:]) 
#            all_geology = torch.cat(all_inv, dim=3).cuda()
#            all_semantic = torch.cat(all_seg, dim=3).cuda()
            predict_inv_70_200 = torch.cat(all_predict_inv, dim=3).cuda()
            predict_semantic_70_200 = torch.cat(all_predict_seg, dim=3).cuda()
#            predict_semantic1 = F.softmax(predict_semantic_70_200,dim = 1)
            _, preds = torch.max(predict_semantic_70_200, 1)
            class_preds = torch.unique(preds)
            #debug()
            predict3 = predict_semantic_70_200.max(1, keepdim=True)[1].byte()
            func.save_mat(predict_inv_70_200.squeeze().cpu().numpy(), observe_path, 'predict')
            func.save_mat2(predict3.squeeze().cpu().numpy(), observe_path, 'semantic')
        all_time=time.time()-end
    
    return all_time,class_preds




all_time,class_preds = valid(valid_loader,G)

#print('loss_l1 avg: {:.6f}, min: {:.6f}, max: {:.6f}'.format(losses_l1.avg, losses_l1.minimum, losses_l1.maximum))
#print('loss_l2 avg: {:.6f}, min: {:.6f}, max: {:.6f}'.format(losses_l2.avg, losses_l2.minimum, losses_l2.maximum))
#print('loss_ssim avg: {:.6f}, min: {:.6f}, max: {:.6f}'.format(losses_ssim.avg, losses_ssim.minimum, losses_ssim.maximum))
#print('loss_mssim avg: {:.6f}, min: {:.6f}, max: {:.6f}'.format(losses_mssim.avg, losses_mssim.minimum, losses_mssim.maximum))
print(class_preds)
print('all_time: {:.6f}'.format(all_time))

