#!/usr/bin/env python2
# -*- coding: utf-8 -*-


"""
Created on Fri Jun 12 18:55:07 2020

@author: ansonwan
"""
import requests,json
 
url = 'http://127.0.0.1:3792/http/query/'
data = {
        "observe_path":"/root/saves/lhc_projects/lhc/code/LSTM/3D_3th_damoxing/New_10m10m10m/test_new_10m/data/water_void_hole_400/1.mat"
    }
 
r = requests.post(url,data=json.dumps(data))
print(r.json())