# -*- coding: utf-8 -*-
"""
Created on Sun Aug 29 20:36:33 2021

@author: liuhanchi
"""


from flask import Flask, request, jsonify
import tomo_inference_vanila_DLAPI as test
import json
app = Flask(__name__)
app.debug = True

@app.route('/http/query/',methods=['post'])
def post_http():
    if  not request.data:   
        return ('fail')
    
    params= request.data.decode('utf-8')
    observe_path = json.loads(params)['observe_path']
    prams = test.valid(observe_path)
    dataPModel = {
        "inv_path":observe_path,
        "seg_path":observe_path,
        "defects_names":prams
    }
    return jsonify(dataPModel)
    
 
if __name__ == '__main__':
    app.run(host='127.0.0.1',port=3792)