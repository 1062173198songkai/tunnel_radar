# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 19:09:15 2020

@author: lenovo
"""
import os
import sys
import time
import random
import socket
import threading
import numpy as np
from datetime import datetime
import torch.nn.functional as F
import scipy.io as sio
import torch
import torch.nn as nn
import torchnet as tnt
import torch.optim as optim
import torch.backends.cudnn as cudnn
from torch.autograd import Variable, Function
from tensorboardX import SummaryWriter
import matplotlib.pyplot as plt
import basic_function as func
import tomo_dataset as dataset
from flask import Flask, request, jsonify
import json
import sys
sys.path.append('../')
app = Flask(__name__)
app.debug = True
import os
from IPython.core import debugger
debug = debugger.Pdb().set_trace

os.environ["CUDA_VISIBLE_DEVICES"] = '0'

args_seed = 666
args_works = 0
observe_path ='D:\\Users\\GPR_inv_ide\\test\\inpu_data\\8.mat'
args_gpu = True
#args_batch = 1
#temporal = 4
mode = 'tomo'
#num_class = 9
if mode == 'tomo':
    import tomo_model_1x1_last as model
else:
    import inversenet_model2 as model

random.seed(args_seed)
torch.manual_seed(args_seed)
torch.cuda.manual_seed_all(args_seed)
#cudnn.deterministic = True
cudnn.benchmark = True
device = torch.device("cuda")
#
path1= 'D:\\Users\\GPR_inv_ide\\test\\Output_data'
#valid_dataset = dataset.TestDatasetFolder(args_valid, flip=False, norm=True)
#valid_loader = torch.utils.data.DataLoader(
#        valid_dataset, batch_size=args_batch, shuffle=False,
#        pin_memory=True, drop_last=False)

if mode == 'tomo':
    G = model.LSTM_Dense_Unet().to(device)      
else:
    NetWorks = model.InverseNet().to(device)    
G = nn.DataParallel(G, device_ids=range(torch.cuda.device_count()))
checkpoint_file_G = torch.load('D:\\Users\\GPR_inv_ide\\model\\minloss_checkpoint.pth.tar')
G.load_state_dict(checkpoint_file_G['state_dict_g'])

########################################################
##################load the data
################
def mat_loader(path):
    try:
        data = sio.loadmat(path,verify_compressed_data_integrity=False) 
        return data 
               
    except Exception:
        print(path)
        
############################################################# 
########################save the data
#####################
def save_mat(data, path, postfix):
    tpath = path[0].replace('valid','valid_predict_mat').replace('test','test_predict_mat').replace('train','train_predict_mat')
    File_Path = tpath[0:tpath.rfind('/')]
    if not os.path.exists(File_Path):
        os.makedirs(File_Path)
    filename = tpath[0:tpath.rfind('.')] + '_' + postfix + '.mat'
    #filename = tpath[0:tpath.rfind('.')] + '.mat'
    sio.savemat(filename, {postfix: data})
##############################################################
#############################save the semantic
######################
def save_mat2(data, path, postfix):
    tpath = path[0].replace('valid','valid_predict_mat_seg').replace('test','test_predict_mat_seg').replace('train','train_predict_mat_seg')
    File_Path = tpath[0:tpath.rfind('/')]
    if not os.path.exists(File_Path):
        os.makedirs(File_Path)
    filename = tpath[0:tpath.rfind('.')] + '_' + postfix + '.mat'
    #filename = tpath[0:tpath.rfind('.')] + '.mat'
    sio.savemat(filename, {postfix: data})


def image_predict_inv(predict_mat, observe_path, postfix):
    predict_mat = predict_mat * 2.4771214
    data = np.array(np.power(10, predict_mat))
    new_file_name = observe_path.split('.')[0][-1]
    # filename = tpath[0:tpath.rfind('.')] + '_' + postfix + '.mat'
    plt.figure(figsize=(1, 1), dpi=512)

    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=0, vmax=20)
    # vmax = np.amax(np.abs(output))
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    # debug()
    # plt.colorbar()
    plt.savefig(path1 + '/%06d' % int(new_file_name) + '_' + postfix + '.jpg')


def image_predict_segment(predict_semantic_mat, observe_path, postfix):
    data = np.array(predict_semantic_mat)
    new_file_name = observe_path.split('.')[0][-1]
    plt.figure(figsize=(1, 1), dpi=512)

    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=0, vmax=8)
    # vmax = np.amax(np.abs(output))
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    # plt.colorbar()
    plt.savefig(path1 + '/%06d' % int(new_file_name) + '_' + postfix + '.jpg')
##############################################################
#############################inference
######################
def valid(G):
    G.eval()
    with torch.no_grad():
        end=time.time()
        #for step, (observe, observe_path) in enumerate(valid_loader):
            #debug()
        observe= mat_loader(observe_path)['E_obs']
        observe = torch.from_numpy(observe)
        observe = torch.unsqueeze(observe,0)
        observe = torch.unsqueeze(observe,0)
        #debug()
        observe = 2*(observe+6.957055)/(6.957055+7.815264)-1
        observe = observe.to(device)
        observe = func.slide_window_slice(observe,window=50,step=10)
        predict,predict_semantic= G(observe)
        _,_,temporal,_,_,= observe.size()
        
        all_predict_inv = []
        all_predict_seg = []
        for t in range(temporal):
            if t !=temporal-1:
                if t ==0:
                    predict_all = predict[:, t,:,:,:,]
                    predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                    predict_clip = predict_all[:,:,:,:20]
                    predict_semantic_clip = predict_semantic_70_all[:,:,:,:20]
                    all_predict_inv.append(predict_clip)
                    all_predict_seg.append(predict_semantic_clip)
                else:
                    predict_all = predict[:, t,:,:,:,]
                    predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                    all_predict_inv.append(0.5*(predict_all[:,:,:,:20]+predict[:, t-1,:,:,:,][:,:,:,20:40]))
                    all_predict_seg.append(0.5*(predict_semantic_70_all[:,:,:,:20]+predict_semantic[:, t-1,:,:,:,][:,:,:,20:40]))
            
            else:
                #debug()
                predict_all = predict[:, t,:,:,:,]
                predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                predict_all1 = 0.5*(predict_all[:,:,:,:20]+predict[:, t-1,:,:,:,][:,:,:,20:40])
                predict_all2 = predict_all[:,:,:,20:]
                predict_semantic_70_all1 = 0.5*(predict_semantic_70_all[:,:,:,:20]+predict_semantic[:, t-1,:,:,:,][:,:,:,20:40])
                predict_semantic_70_all2 = predict_semantic_70_all[:,:,:,20:]
                all_predict_inv.append(predict_all1)
                all_predict_inv.append(predict_all2)
                all_predict_seg.append(predict_semantic_70_all1)
                all_predict_seg.append(predict_semantic_70_all2)
        predict_inv_70_200 = torch.cat(all_predict_inv, dim=3).cuda()
        predict_semantic_70_200 = torch.cat(all_predict_seg, dim=3).cuda()
        _, preds = torch.max(predict_semantic_70_200, 1)
        class_preds = torch.unique(preds)
        predict3 = predict_semantic_70_200.max(1, keepdim=True)[1].byte()
        func.save_mat(predict_inv_70_200.squeeze().cpu().numpy(), observe_path, 'predict')
        func.save_mat2(predict3.squeeze().cpu().numpy(), observe_path, 'semantic')

        image_predict_inv(predict_inv_70_200.squeeze().cpu().numpy(), observe_path, 'inv')
        image_predict_segment(predict3.squeeze().cpu().numpy(), observe_path, 'semantic')
        all_time=time.time()-end
    
    return all_time,class_preds


@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        params = request.data.decode('utf-8')
        # observe_path1 = json.loads(params)['observe_path']
        observe_path1 = request.json['observe_path']
        debug()
        defect_names = get_prediction(observe_path1)
        # jsonresult = {}
        # jsonresult['base64'] = image_to_base64(img_PIL)
        dataPModel = {"class_result": defect_names}
        print("Response data:", dataPModel)
        return jsonify(dataPModel)


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=3792)


