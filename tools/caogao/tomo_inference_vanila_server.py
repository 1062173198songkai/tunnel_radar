# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 08:16:59 2021

@author: liuhanchi
"""

import os
import sys
import time
import json
import random
import socket
import threading
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torchnet as tnt
import torch.optim as optim
import torch.nn.functional as F
import torch.backends.cudnn as cudnn
from torch.autograd import Variable, Function
from tensorboardX import SummaryWriter
from datetime import datetime
import scipy.io as sio
import basic_function as func
import tomo_dataset as dataset
import tomo_model_1x1_last as model
import torchvision.transforms as transforms
from IPython.core import debugger
from torchvision import models



debug = debugger.Pdb().set_trace
np.set_printoptions(threshold=np.inf)

from flask import Flask, jsonify, request
app = Flask(__name__)


os.environ["CUDA_VISIBLE_DEVICES"] = '3'

args_seed = 666
args_works = 0
#observe_path ='/root/saves/lhc_projects/lhc/code/LSTM/3D_3th_damoxing/New_10m10m10m/test_new_10m/data/water_void_hole_400/1.mat'
path1= '/root/saves/lhc_projects/lhc/code/LSTM/3D_3th_damoxing/New_10m10m10m/images_predict'
args_gpu = True


random.seed(args_seed)
torch.manual_seed(args_seed)
cudnn.benchmark = True
device = torch.device("cuda")

G = model.LSTM_Dense_Unet().to(device)         
G = nn.DataParallel(G, device_ids=range(torch.cuda.device_count()))
checkpoint_file_G = torch.load('/root/files/lhc/code/LSTM/A_ontime/L1_SSIM/Suiji_Sequence_3thmode/LSTM_TWICE_CELL_fuyong/model0/tomo_main_vanila_10x1_1_my_final_model/minloss_checkpoint.pth.tar')
G.load_state_dict(checkpoint_file_G['state_dict_g'])

class_names= ['Iron','Background', 'Interface', 'Anhydrous crack', 'Anhydrous void', 'Anhydrous delamination', 
               'Water-bearing delamination','Water-bearing crack','Water-bearing void', ]
########################################################
##################load the data
################
def mat_loader(path):
    try:
        data = sio.loadmat(path,verify_compressed_data_integrity=False) 
        return data 
               
    except Exception:
        print(path)
        
#############################################################   
def image_predict_inv(predict_mat,observe_path,postfix):
    predict_mat = predict_mat*2.4771214
    data = np.array(np.power(10,predict_mat))
    new_file_name = observe_path.split('.')[0][-1]
    #filename = tpath[0:tpath.rfind('.')] + '_' + postfix + '.mat'
    plt.figure(figsize=(1, 1), dpi=512)

    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=0, vmax=20)
    # vmax = np.amax(np.abs(output))
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    #debug()
    # plt.colorbar()
    plt.savefig(path1 + '/%06d' % int(new_file_name)+'_' + postfix+'.jpg')
    
    
def image_predict_segment(predict_semantic_mat,observe_path,postfix):
    data = np.array(predict_semantic_mat)
    new_file_name = observe_path.split('.')[0][-1]
    plt.figure(figsize=(1, 1), dpi=512)

    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=0, vmax=8)
    # vmax = np.amax(np.abs(output))
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    # plt.colorbar()
    plt.savefig(path1 + '/%06d' % int(new_file_name)+'_' + postfix+'.jpg')
    
##############################################################
    
def slide_window_slice(observe,window=50,step=10):
    _,_,h,w = observe.size()
    observe_clip = []
    observe_fill = []
    if (w-window)%step==0:
        times = int(((w-window)/step)+1)
        for j in range(times):
            observe_slice= observe[:,:,:,0+j*step:window+j*step]
            observe_clip.append(observe_slice)
        #debug()
    else:
        #debug()
        com_slice = (w-window)//step
        remain_slice = (w-window)%step
        observe_fill.append(observe)
        for i in range(step-remain_slice):
            observe_fill.append(observe[:,:,:,-1].view(1,1,h,1))
        observe_filled = torch.cat(observe_fill, dim=3).cuda()
        _,_,hh,ww = observe_filled.size()
        times = int(((ww-window)/step)+1)
        assert com_slice+2 == times
        for j in range(times):
            observe_slice= observe_filled[:,:,:,0+j*step:window+j*step]
            observe_clip.append(observe_slice)
        
    return torch.stack(observe_clip, dim=2).cuda() 



def get_prediction(observe_path1):
    # -*- coding: utf-8 -*-
    G.eval()
    with torch.no_grad():
        #end=time.time()
        observe= mat_loader(observe_path1)['E_obs']
        observe = observe.to(device)
        observe = slide_window_slice(observe,window=50,step=10)
        predict,predict_semantic,= G(observe)
        _,_,temporal,_,_,= observe.size()
        #debug()


        all_predict_inv = []
        all_predict_seg = []
        defect_names = []
        for t in range(temporal):
            if t !=temporal-1:
                if t ==0:
                    predict_all = predict[:, t,:,:,:,]
                    predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                    all_predict_inv.append(predict_all[:,:,:,:20])
                    all_predict_seg.append(predict_semantic_70_all[:,:,:,:20])
                else:
                    predict_all = predict[:, t,:,:,:,]
                    predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                    all_predict_inv.append(0.5*(predict_all[:,:,:,:20]+predict[:, t-1,:,:,:,][:,:,:,20:40]))
                    all_predict_seg.append(0.5*(predict_semantic_70_all[:,:,:,:20]+predict_semantic[:, t-1,:,:,:,][:,:,:,20:40]))
            
            else:
                predict_all = predict[:, t,:,:,:,]
                predict_semantic_70_all= predict_semantic[:, t,:,:,:,]
                predict_all1 = 0.5*(predict_all[:,:,:,:20]+predict[:, t-1,:,:,:,][:,:,:,20:40])
                predict_all2 = predict_all[:,:,:,20:]
                predict_semantic_70_all1 = 0.5*(predict_semantic_70_all[:,:,:,:20]+predict_semantic[:, t-1,:,:,:,][:,:,:,20:40])
                predict_semantic_70_all2 = predict_semantic_70_all[:,:,:,20:]
                all_predict_inv.append(predict_all1)
                all_predict_inv.append(predict_all2)
                all_predict_seg.append(predict_semantic_70_all1)
                all_predict_seg.append(predict_semantic_70_all2)
                
        predict_inv_70_200 = torch.cat(all_predict_inv, dim=3).cuda()
        predict_semantic_70_200 = torch.cat(all_predict_seg, dim=3).cuda()
        predict3 = predict_semantic_70_200.max(1, keepdim=True)[1].byte()
        image_predict_inv(predict_inv_70_200.squeeze().cpu().numpy(), observe_path, 'inv')
        image_predict_segment(predict3.squeeze().cpu().numpy(), observe_path, 'semantic')
        
        
        _, preds = torch.max(predict_semantic_70_200, 1)
        class_preds = torch.unique(preds)
        #debug()
        for  i in class_preds:
            #debug()
            defect_name = class_names[int(i)]
            defect_names.append(defect_name)
        
    return defect_names

@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        params= request.data.decode('utf-8')
        #observe_path1 = json.loads(params)['observe_path']
        observe_path1 = request.json['observe_path']
        debug()
        defect_names = get_prediction(observe_path1)
        #jsonresult = {}
        #jsonresult['base64'] = image_to_base64(img_PIL)
        dataPModel = {"class_result": defect_names}
        print("Response data:", dataPModel)
        return jsonify(dataPModel)
        

if __name__ == '__main__':
    app.run(host='127.0.0.1',port=3792)
