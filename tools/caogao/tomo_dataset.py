# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 19:56:50 2020

@author: lenovo
"""
import torch.nn as nn
import torch
import torch.utils.data as data
import pdb
import os
import os.path
import numpy as np
import scipy.io as sio
from IPython.core import debugger
import random
import torch.backends.cudnn as cudnn
from torch.utils.data.sampler import Sampler
import itertools
from torch.utils.data.sampler import SubsetRandomSampler

debug = debugger.Pdb().set_trace
cuda = True if torch.cuda.is_available() else False

#import matlab.engine
#eng = matlab.engine.start_matlab()

IMG_EXTENSIONS = ['.mat']

class JointCompose(object):
    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, data):
        for t in self.transforms:
            data = t(*data)
        return data



class JointRandomFlip(object):
    def __init__(self, rand=True):
        self.rand = rand

    def __call__(self, observe, geology,segmantic):
        random = torch.rand(1)
        if self.rand and random < 0.5:
            geology = geology.flip(2)  #flip?
            observe = observe.flip(2) 
            segmantic = segmantic.flip(2) 
        
        return observe, geology,segmantic
class JointNormalize(object):
    def __init__(self, norm=True):
        self.norm = norm

    def __call__(self, observe, geology,segmantic):
        if self.norm:
            #geology = (geology-1.0)/299.0   ##1500 2500?################after 5_28 use it
            #geology = (geology-1.0)/9.0########################after 5-22xiugai
            #observe = 2*(observe+2.27183)/(2.27183+7.603452)-1
            #mean = np.mean(observe.detach().numpy())
            #std = np.std(observe.detach().numpy())
            #observe = (observe-mean)/std
            #geology = np.log10(geology)
            #geology = (geology-0)/2.4771214
            #geology = (geology-1.0)/299.0
            #observe = 2*(observe+6.957055)/(6.957055+7.815264)-1################7-15riwan
            observe = observe
            geology=geology
            segmantic=segmantic
        return observe, geology,segmantic


def is_mat_file(filename):
    """Checks if a file is an image.
    Args:
        filename (string): path to a file
    Returns:
        bool: True if the filename ends with a known image extension
    """
    filename_lower = filename.lower()
    return any(filename_lower.endswith(ext) for ext in IMG_EXTENSIONS)

"""
def mat_loader(path):
    try:
        data = sio.loadmat(path)             
    except Exception:
        print path   
        f = open('error.txt','a+');
        f.write(path);
        f.write("\n");
        f.close();
        num = int(path[-5]);
        listpath = list(path);
        listpath[-5] = str((num+1)%10);
        path = ''.join(listpath); 
        data = sio.loadmat(path)
        print('Error:', Exception)
        
    #finally:
    return data
"""    

def mat_loader(path):
    try:
        data = sio.loadmat(path,verify_compressed_data_integrity=False) 
        return data 
               
    except Exception:
        print(path)
"""       f = open('error2.txt','a+');
       f.write(path);
       f.write("\n");
       f.close();
       num = int(path[-5]);
       listpath = list(path);
       listpath[-5] = str((num+1)%10);
       path = ''.join(listpath); 
       data = sio.loadmat(path)
       print('Error:', Exception)
"""       
   #finally:
    #return data
def get_clip(observe,geology,segmantic):
    _,seq_lenth,_,_, = observe.size()
    observe_list = []
    geology_list = []
    segmantic_list = []
    randstart = random.randint(0, seq_lenth-3)#temporal = 4
    for i in range(randstart,randstart+3):
        #debug()
        observe_list.append(observe[:,i,:,:,])
        geology_list.append(geology[:,i,:,:,])
        segmantic_list.append(segmantic[:,i,:,:,])
    return torch.stack(observe_list, dim=1),torch.stack(geology_list, dim=1),torch.stack(segmantic_list, dim=1)
        
class DatasetFolder(data.Dataset):
    def __init__(self, root, flip=True, norm=True):
        #pdb.set_trace()
        self.flip = flip
        self.root = root
        self.norm = norm
        self.data = data
        self.obs_names = self.get_data(self.root+'/data')
        self.geo_names = self.get_con(self.obs_names)
        self.seg_names = self.get_con2(self.obs_names)
        #self.clip = self.get_clip()
    def get_data(self, path):
        groups = [
            d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))
        ]
        data_list = []
        for i in sorted(groups):
            group_path = os.path.join(path, i)
            if not os.path.isdir(group_path):
                continue
            data = [
                os.path.join(group_path, d)
                for d in os.listdir(group_path) if is_mat_file(d)
                ]
            data_list += data
        return data_list

    def get_con(self, data_list):
        con_list = [i.replace("data", "model") for i in data_list]
        return con_list
    def get_con2(self, data_list):
        con_list = [i.replace("data", "omodel") for i in data_list]
        return con_list

    def __getitem__(self, index):
        #pdb.set_trace()      
        observe_path = self.obs_names[index]
        geology_path = self.geo_names[index]
        seg_path = self.seg_names[index]
        geology = mat_loader(geology_path)['index']
        observe = mat_loader(observe_path)['E_obs']
        segmantic = mat_loader(seg_path)['model']
        #debug()
        observe = torch.from_numpy(observe)
        #debug()
        #observe = np.transpose(observe)
        observe = torch.unsqueeze(observe,0)        
        geology = torch.from_numpy(geology)
        #debug()
        geology = torch.unsqueeze(geology,0)
        segmantic = torch.from_numpy(segmantic)
        #debug()
        segmantic = torch.unsqueeze(segmantic,0).float()
        #debug()
        #debug()
        transform = JointCompose([JointRandomFlip(self.flip), JointNormalize(self.norm)])
        observe, geology,segmantic = transform([observe, geology,segmantic])
        #observe = observe[:,:,:]  ## observe?? shot , rec ,time  quanqu 

        return observe, geology, observe_path, geology_path,segmantic

    def __len__(self):
        return len(self.geo_names)
class TestDatasetFolder(data.Dataset):
    def __init__(self, root, flip=True, norm=True):
        #pdb.set_trace()
        self.flip = flip
        self.root = root
        self.norm = norm
        self.data = data
        self.obs_names = self.get_data(self.root+'/data')
        self.geo_names = self.get_con(self.obs_names)
        self.seg_names = self.get_con2(self.obs_names)
        #self.clip = self.get_clip()
    def get_data(self, path):
        groups = [
            d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))
        ]
        data_list = []
        for i in sorted(groups):
            group_path = os.path.join(path, i)
            if not os.path.isdir(group_path):
                continue
            data = [
                os.path.join(group_path, d)
                for d in os.listdir(group_path) if is_mat_file(d)
                ]
            data_list += data
        return data_list

    def get_con(self, data_list):
        con_list = [i.replace("data", "model") for i in data_list]
        return con_list
    def get_con2(self, data_list):
        con_list = [i.replace("data", "omodel") for i in data_list]
        return con_list

    def __getitem__(self, index):
        #pdb.set_trace()      
        observe_path = self.obs_names[index]
        geology_path = self.geo_names[index]
        seg_path = self.seg_names[index]
        geology = mat_loader(geology_path)['index']
        observe = mat_loader(observe_path)['E_obs']
        segmantic = mat_loader(seg_path)['model']
        #debug()
        observe = torch.from_numpy(observe)
        #debug()
        #observe = np.transpose(observe)
        observe = torch.unsqueeze(observe,0)        
        geology = torch.from_numpy(geology)
        #debug()
        geology = torch.unsqueeze(geology,0)
        segmantic = torch.from_numpy(segmantic)
        #debug()
        segmantic = torch.unsqueeze(segmantic,0).float()
        #debug()
        #debug()
        transform = JointCompose([JointRandomFlip(self.flip), JointNormalize(self.norm)])
        observe, geology,segmantic = transform([observe, geology,segmantic])
        #observe = observe[:,:,:]  ## observe?? shot , rec ,time  quanqu 

        return observe, geology, observe_path, geology_path,segmantic

    def __len__(self):
        return len(self.geo_names)


