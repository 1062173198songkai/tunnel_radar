import yaml
import logging
import os
import typing

logger = logging.getLogger(__name__)

APPLICATION_CONFIG: typing.Optional[typing.Dict] = None


def load_config():
    logger.info(f"{os.getcwd()}")
    with open(f"{os.getcwd()}/application.yml", "r") as config_file:
        config = yaml.safe_load(config_file)
        print(f"config:{config}")
    return config


APPLICATION_CONFIG = load_config()
