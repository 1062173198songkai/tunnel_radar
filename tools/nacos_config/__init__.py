import typing

import nacos
import yaml
import logging
from nacos_config.my_wrapper_dict import MyDict
from local_config import APPLICATION_CONFIG

logger = logging.getLogger(__name__)

# 连接地址
SERVER_ADDRESSES = "101.43.133.171"
SERVER_PORT = '8848'

# 命名空间
NAMESPACE = APPLICATION_CONFIG.get("nacos-namespace")
if not NAMESPACE:
    NAMESPACE = "e8c5f371-d2a4-4859-9164-8e749b6377e3"
# 账号信息
USERNAME = 'nacos'
PASSWORD = 'nacos'

# nacos_config_week: weakref.ref[typing.Dict] = weakref.ref({})
nacos_config: typing.Optional[MyDict] = None
# 创建一个连接对象
client = nacos.NacosClient(f'{SERVER_ADDRESSES}:{SERVER_PORT}', namespace=NAMESPACE, username=USERNAME,
                           password=PASSWORD)


# 初始化
def init(data_id, group):
    config = client.get_config(data_id, group)
    print(config)
    # 配置数据解析（YAML）
    config_data = yaml.load(config, Loader=yaml.FullLoader)
    global nacos_config
    nacos_config = MyDict(d=config_data)
    print(config_data)


# 服务id（键）
data_id = "tunnel_py_config"

# 分组名称，默认为：DEFAULT_GROUP
group = "DEFAULT_GROUP"

# 初始化解析
init(data_id, group)


# Nacos数据变动时触发
def nacos_data_change_callback(config):
    # 数据解析
    nacos_data = yaml.load(config['content'], Loader=yaml.FullLoader)
    # 读取键值
    # result = nacos_data['arg1']['arg2']
    global nacos_config
    nacos_config = MyDict(d=nacos_data)
    logger.info(f"nacos_data:{nacos_data}")


# 监听Nacos数据变动
def add_nacos_listener(data_id, group):
    client.add_config_watcher(data_id=data_id, group=group, cb=nacos_data_change_callback)


# 添加监听事件
def add_listener():
    add_nacos_listener(data_id, group)
