import typing


class MyDict(dict):
    def __init__(self, d: typing.Dict):
        super().__init__()
        for key in d:
            self[key] = d[key]

    def get_or_default(self, key: str, default: typing.Any):
        if key in self and self[key] is not None:
            return self[key]
        else:
            return default
