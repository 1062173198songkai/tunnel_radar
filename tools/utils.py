import os


def create_folder_if_not_exist(folder_name: str) -> None:
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
