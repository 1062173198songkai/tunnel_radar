# -*- coding: utf-8 -*-
"""
Created on Sun Aug 29 10:20:59 2021

@author: lenovo
"""
import random
import time
import sys

# import basic_function as func
import matplotlib
import numpy as np
import scipy.io as sio
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn

matplotlib.use('agg')
torch.device("cpu")

import matplotlib.pyplot as plt
import os
import platform
from torchsummary import summary

from flask import Flask

app = Flask(__name__)
app.debug = True

from IPython.core import debugger
from nacos_config import nacos_config, add_listener

debug = debugger.Pdb().set_trace

os.environ["CUDA_VISIBLE_DEVICES"] = '0'

args_seed = 666
args_works = 0
device_dict = {
    "CPU": "cpu",
    "GPU": "cuda",
}
# mode = sys.argv[0] if sys.argv is not None and len(sys.argv) >= 1 else "CPU"
print(torch.__version__)
output_data_path = nacos_config.get_or_default(key='static-path', default='/Users/admin/PycharmProjects/pythonProject2/tools') + os.sep
checkpoint_file_path = sys.argv[2] if sys.argv is not None and len(sys.argv) >= 3 else "model/lastcheckpoint.pth.tar"
if sys.platform == 'win32':
    output_data_path = output_data_path.replace("/", "\\")
    checkpoint_file_path = checkpoint_file_path.replace("/", "\\")

if sys.platform == 'darwin':
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

# observe_path ='/root/saves/lhc_projects/lhc/code/LSTM/3D_3th_damoxing/New_10m10m10m/test_new_10m/data/water_void_hole_400/1.mat'
# path1 = 'D:\\Users\\GPR_inv_ide\\test\\Output_data' if platform.system() != "Darwin" else "test/Output_data"
path1 = output_data_path
# args_gpu = mode == "GPU"
# args_batch = 1
# temporal = 4
mode = 'tomo'
# num_class = 9
if mode == 'tomo':
    from . import tomo_model_1x1_last as model
    # import tomo_model_1x1_last as model
else:
    from . import inversenet_model2 as model
    # import inversenet_model2 as model

random.seed(args_seed)
torch.manual_seed(args_seed)
torch.cuda.manual_seed_all(args_seed)
# cudnn.deterministic = True
cudnn.benchmark = True
print(f" platform.system():{platform.system()}")
device = torch.device("cuda")
#
# valid_dataset = dataset.TestDatasetFolder(args_valid, flip=False, norm=True)
# valid_loader = torch.utils.data.DataLoader(
#        valid_dataset, batch_size=args_batch, shuffle=False,
#        pin_memory=True, drop_last=False)

if mode == 'tomo':
    G = model.LSTM_Dense_Unet().to(device)
else:
    NetWorks = model.InverseNet().to(device)
if mode.upper() == "CPU":
    if G is None:
        G = model.LSTM_Dense_Unet().to(device)
elif mode.upper() == "GPU":
    G = nn.DataParallel(G, device_ids=range(torch.cuda.device_count()))
# checkpoint_file_G = torch.load(
#     f='D:\\Users\\GPR_inv_ide\\model\\minloss_checkpoint.pth.tar' if platform.system() != "Darwin" else "model/lastcheckpoint.pth.tar",
#     map_location=device)
checkpoint_file_G = torch.load(f=checkpoint_file_path, map_location=device)
if mode.upper() == "CPU":
    new_state_dict = {key.replace("module.", ""): value for key, value in checkpoint_file_G['state_dict_g'].items()}
    G.load_state_dict(new_state_dict)
else:
    new_state_dict = {key.replace("module.", ""): value for key, value in checkpoint_file_G['state_dict_g'].items()}
    # G.load_state_dict(checkpoint_file_G['state_dict_g'])
    G.load_state_dict(new_state_dict)


class_names = ['Iron', 'Background', 'Interface', 'Anhydrous crack', 'Anhydrous void', 'Anhydrous delamination',
               'Water-bearing delamination', 'Water-bearing crack', 'Water-bearing void', ]


def adopt_mat_data_dict_decorator(func):
    def wrapper(*args, **kwargs):
        data = func(*args, **kwargs)
        if 'E_obs' not in data:
            data['E_obs'] = data['predict']
        return data
    return wrapper
########################################################
##################load the data
################
@adopt_mat_data_dict_decorator
def mat_loader(path):
    try:
        data = sio.loadmat(path, verify_compressed_data_integrity=False)
        print(f"data:{data}")
        return data

    except Exception:
        print(path)


############################################################# 
#  
def image_predict_data(id, datamat, observe_path, postfix):
    data = np.array(datamat)
    new_file_name = observe_path.split('.')[0][-1]
    plt.figure(figsize=(data.shape[1]/(data.shape[0]*162.8/256), 1), dpi=2048)
    vmax = np.amax(np.abs(data))
    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=-vmax/100, vmax=vmax/100)
    
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    # plt.colorbar()
    # plt.savefig(path1 + '\\%06d' % int(new_file_name) + '_' + postfix + '.jpg')
    # ide_path = path1 + '\\%06d' % int(new_file_name) + '_' + postfix + '.jpg'
    plt.savefig(path1 + id + new_file_name + '_' + postfix + '.jpg')
    ide_path = path1+ id + new_file_name + '_' + postfix + '.jpg'
    return ide_path

def image_predict_inv(id, predict_mat, observe_path, postfix):
    predict_mat = predict_mat * 2.4771214
    data = np.array(np.power(10, predict_mat))
    new_file_name = observe_path.split('.')[0][-1]
    # filename = tpath[0:tpath.rfind('.')] + '_' + postfix + '.mat'
    plt.figure(figsize=(data.shape[1]/data.shape[0], 1), dpi=2048)
    print(f"type of data:{type(data)}")
    print(f"shape:{data.shape}")
    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=0, vmax=20)
    # vmax = np.amax(np.abs(output))
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    # debug()
    # plt.colorbar()
    print(f"new_file_name:{new_file_name}")
    # plt.savefig(path1 + '\\%06d' % int(new_file_name) + '_' + postfix + '.jpg')
    # inv_path = path1 + '\\%06d' % int(new_file_name) + '_' + postfix + '.jpg'
    plt.savefig(path1+ id + new_file_name + '_' + postfix + '.jpg')
    inv_path = path1+ id + new_file_name + '_' + postfix + '.jpg'
    return inv_path


def image_predict_segment(id, predict_semantic_mat, observe_path, postfix):
    data = np.array(predict_semantic_mat)
    new_file_name = observe_path.split('.')[0][-1]
    plt.figure(figsize=(data.shape[1]/data.shape[0], 1), dpi=2048)

    plt.imshow(data, extent=[0, data.shape[1], data.shape[0], 0], interpolation='nearest',
               aspect='auto',
               cmap='jet', vmin=0, vmax=8)
    # vmax = np.amax(np.abs(output))
    # print(output)
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    # plt.colorbar()
    # plt.savefig(path1 + '\\%06d' % int(new_file_name) + '_' + postfix + '.jpg')
    # ide_path = path1 + '\\%06d' % int(new_file_name) + '_' + postfix + '.jpg'
    plt.savefig(path1+ id + new_file_name + '_' + postfix + '.jpg')
    ide_path = path1+ id + new_file_name + '_' + postfix + '.jpg'
    return ide_path


def slide_window_slice(observe, window=50, step=10):
    _, _, h, w = observe.size()
    observe_clip = []
    observe_fill = []
    if (w - 50) % 10 == 0:
        times = int(((w - window) / step) + 1)
        for j in range(times):
            observe_slice = observe[:, :, :, 0 + j * step:window + j * step]
            observe_clip.append(observe_slice)
        # debug()
    else:
        com_slice = (w - 50) // 10
        remain_slice = (w - 50) % 10
        observe_fill.append(observe)
        # debug()
        for i in range(step - remain_slice):
            observe_fill.append(observe[:, :, :, -1].unsqueeze(3))
        # observe_filled = torch.cat(observe_fill, dim=3).cuda()
        observe_filled = torch.cat([obs_tensor.cpu() for obs_tensor in observe_fill], dim=3)
        # debug()
        _, _, hh, ww = observe_filled.size()
        times = int(((ww - window) / step) + 1)
        assert com_slice + 2 == times
        for j in range(times):
            observe_slice = observe_filled[:, :, :, 0 + j * step:window + j * step]
            observe_clip.append(observe_slice)
    # 用策略模式
    # torch.stack(observe_clip, dim=2).cuda()
    print(f"{device}")
    return torch.stack(observe_clip, dim=2).to(device)


##############################################################
#############################inference
######################
def valid(id, observe_path):
    G.eval()
    with torch.no_grad():
        end = time.time()
        # for step, (observe, observe_path) in enumerate(valid_loader):
        # debug()
        tem_observe_data = mat_loader(observe_path)
        # print(type(tem_observe_data))
        observe = mat_loader(observe_path)['E_obs']
        observe = torch.from_numpy(observe)
        observe = torch.unsqueeze(observe, 0)
        observe = torch.unsqueeze(observe, 0)
        # debug()
        observe = 2 * (observe + 6.957055) / (6.957055 + 7.815264) - 1
        # print(f"device: {device}")
        observe = observe.to(device)
        summary(G, (1, 1, 60, 140))
        torch.device("cuda")
        observe = slide_window_slice(observe, window=50, step=10)
        # debug()
        predict, predict_semantic = G(observe)
        _, _, temporal, _, _, = observe.size()

        all_predict_inv = []
        all_predict_seg = []
        defect_names = []
        for t in range(3):
            if t != 3 - 1:
                if t == 0:
                    predict_all = predict[:, t, :, :, :, ]
                    predict_semantic_70_all = predict_semantic[:, t, :, :, :, ]
                    predict_clip = predict_all[:, :, :, :20]
                    predict_semantic_clip = predict_semantic_70_all[:, :, :, :20]
                    all_predict_inv.append(predict_clip)
                    all_predict_seg.append(predict_semantic_clip)
                else:
                    predict_all = predict[:, t, :, :, :, ]
                    predict_semantic_70_all = predict_semantic[:, t, :, :, :, ]
                    all_predict_inv.append(
                        0.5 * (predict_all[:, :, :, :20] + predict[:, t - 1, :, :, :, ][:, :, :, 20:40]))
                    all_predict_seg.append(0.5 * (
                            predict_semantic_70_all[:, :, :, :20] + predict_semantic[:, t - 1, :, :, :, ][:, :, :,
                                                                    20:40]))

            else:
                # debug()
                predict_all = predict[:, t, :, :, :, ]
                predict_semantic_70_all = predict_semantic[:, t, :, :, :, ]
                predict_all1 = 0.5 * (predict_all[:, :, :, :20] + predict[:, t - 1, :, :, :, ][:, :, :, 20:40])
                predict_all2 = predict_all[:, :, :, 20:]
                predict_semantic_70_all1 = 0.5 * (
                        predict_semantic_70_all[:, :, :, :20] + predict_semantic[:, t - 1, :, :, :, ][:, :, :,
                                                                20:40])
                predict_semantic_70_all2 = predict_semantic_70_all[:, :, :, 20:]
                all_predict_inv.append(predict_all1)
                all_predict_inv.append(predict_all2)
                all_predict_seg.append(predict_semantic_70_all1)
                all_predict_seg.append(predict_semantic_70_all2)

        # predict_inv_70_200 = torch.cat(all_predict_inv, dim=3).cuda()
        predict_inv_70_200 = torch.cat(all_predict_inv, dim=3).to(device)
        # predict_semantic_70_200 = torch.cat(all_predict_seg, dim=3).cuda()
        predict_semantic_70_200 = torch.cat(all_predict_seg, dim=3).to(device)
        predict3 = predict_semantic_70_200.max(1, keepdim=True)[1].byte()
        inv_save_path = image_predict_inv(id, predict_inv_70_200.squeeze().cpu().numpy(), observe_path, 'inv')
        ide_save_path = image_predict_segment(id, predict3.squeeze().cpu().numpy(), observe_path, 'semantic')

        _, preds = torch.max(predict_semantic_70_200, 1)
        class_preds = torch.unique(preds)
        # debug()
        for i in class_preds:
            # debug()
            defect_name = class_names[int(i)]
            defect_names.append(defect_name)
        # print(defect_names)

        all_time = time.time() - end

    return inv_save_path, ide_save_path, defect_names


if __name__ == '__main__':
    inv_save_path, ide_save_path, defect_names = valid(matPath)
